" .vimrc
" Author: Rafa Garcia <rafa@brainbeat.co.uk>

" Main
set nocompatible
call pathogen#runtime_append_all_bundles()
filetype indent on
filetype plugin on
syntax enable
set autoindent

" Colors
hi Normal         guifg=#E6E1DC guibg=#2B2B2B ctermfg=white
hi Cursor         guifg=#000000 guibg=#FFFFFF ctermfg=0 ctermbg=15	
hi CursorLine     guibg=#333435 ctermbg=235 cterm=NONE
hi CursorColumn   guibg=#333435 ctermbg=235 cterm=NONE
hi Search         guibg=#5A647E ctermfg=NONE ctermbg=236 cterm=underline
hi Visual         guibg=#5A647E ctermbg=60
hi LineNr         guifg=#888888 ctermfg=242
hi StatusLine     guibg=#414243 gui=NONE guifg=#E6E1DC
hi StatusLineNC   guibg=#414243 gui=NONE
hi VertSplit      guibg=#414243 gui=NONE guifg=#414243 ctermfg=235 ctermbg=white
hi CursorLineNr   guifg=#bbbbbb ctermfg=248 guibg=#333435 ctermbg=235
hi ColorColumn    guibg=#333435 ctermbg=235

" UI Layout
set number                          " show line numbers
set list                            " show invisibles
set showcmd                         " show command in bottom bar
set cursorline                      " highlight current line
set cursorcolumn                    " highlight current column
set colorcolumn=80                  " show ruler at column 80
set lazyredraw
set showmatch                       " higlight matching parenthesis
set listchars=tab:▸\ ,eol:¬,trail:• " use TextMate-ish list characters
set fillchars=diff:⣿,vert:│         " box-drawing chars for splits
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)

" Spaces & tabs
set tabstop=4                       " 4-space tab
set softtabstop=4                   " 4-space tab
set shiftwidth=4
set expandtab                       " use spaces for tabs

" Wildmenu completion
set wildmenu
set wildmode=list:longest

" Search
set ignorecase                      " ignore case when searching
set incsearch                       " search as characters are entered
set hlsearch                        " highlight all matches

" Backups
set backup                          " enable backups
set noswapfile
set undodir=~/.vim/tmp/undo//       " undo files
set backupdir=~/.vim/tmp/backup//   " backups

" Misc
set encoding=utf-8
set spelllang=en_gb
set ttyfast                         " faster redraw
set backspace=indent,eol,start
" " Time out on key codes but not mappings.
set notimeout
set ttimeout
set ttimeoutlen=10

" Mappings
" " Leader Shortcuts
let mapleader=","

" " " Toggle `set list`
noremap <leader>l :set list!<CR>

" " " Toggle `set paste`
noremap <leader>p :set paste!<CR>

" " " Toggle spell checking
noremap <silent> <leader>s :set spell!<CR>

" " Disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" " Move around splits with <C-hjkl>
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" " Move through open buffers
noremap <C-p> :bprevious<CR> 
noremap <C-n> :bnext<CR>

" " Move through autocomplete box
inoremap <expr> <C-j> ("\<C-n>")
inoremap <expr> <C-k> ("\<C-p>")

" " Clear search buffer when hitting return
nnoremap <CR> :nohlsearch<CR>

" AutoGroups
if has("autocmd")
  " Enable file type detection
  filetype on

  autocmd FileType make setlocal ts=8 sts=8 sw=8 noexpandtab
  autocmd Filetype ruby setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType css setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType javascript setlocal ts=4 sts=4 sw=4 noexpandtab

  " Treat .rss files as XML
  autocmd BufNewFile,BufRead *.rss setfiletype xml
endif

if exists('+colorcolumn')
  " fill column block outside the ruler
  execute "set colorcolumn=" . join(range(81,335), ',')
endif

" Ctrl-P
let g:ctrlp_map = '<leader>,'
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_jump_to_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_match_window_reversed = 1
let g:ctrlp_split_window = 0
let g:ctrlp_max_height = 20

" The Silver Searcher
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_use_caching = 0
endif
